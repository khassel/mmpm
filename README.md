### This repo contains a Docker Setup for

# **MMPM - The MagicMirror Package Manager**

MMPM is a package manager for the [MagicMirror² Project](https://github.com/MichMich/MagicMirror).

For more info visit the [Project Website](https://github.com/Bee-Mar/mmpm).

# Restrictions

❌ Some actions in the Control Center of MMPM are not supported. Functionality which needs access to the host (partially root access) is unwanted from inside a container.

# Installation prerequisites

## Docker

- [Docker](https://docs.docker.com/engine/installation/)
- to run `docker` commands without needing `sudo` please refer to the [linux postinstall documentation](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
- as we are using `docker compose` commands the compose plugin must be installed. If missing you find [here](https://docs.docker.com/compose/install/linux/) instructions how to install it.

## Running MagicMirror

MagicMirror must run and the url must be reachable from the machine where mmpm is running.

# Installation of this Repository

Copy the `docker-compose.yml` file in a directory of your pi, e.g. `~/mmpm`

Adjust the `docker-compose.yml` file for your needs (see explanations as comments in the file).

For starting `mmpm` go into the directory with the `docker-compose.yml` file and execute `docker-compose up -d`, for stopping execute `docker-compose down`.
You can see the logs with `docker logs mmpm`.

Use a web browser for accessing the application under `http://ip_of_your_pi:7890/`, e.g. `http://192.168.0.11:7890/`.

> The container is configured to restart automatically so after executing `docker-compose up -d` it will restart with every reboot of your pi.

# Installation of MMPM as module in MagicMirror

While starting the MMPM container it will install MMPM as module in MagicMirror:

- install MMPM in the MagicMirror `modules` folder
- insert a MMPM config section in the MagicMirror `config.js` (if not already exists)

> If MagicMirror is running while doing this the first time, it must be restarted to apply the changes.

# Running MagicMirror and MMPM as docker container

It make sense to run both applications as container, you can use my [docker setup for MagicMirror](https://gitlab.com/khassel/magicmirror) for this. 

You find the `docker-compose.yml` files for this setup here:

* setup for raspberry pi: [rpi_mmpm.yml](https://gitlab.com/khassel/magicmirror/-/blob/master/run/rpi_mmpm.yml)
* setup for serveronly mode: [serveronly_mmpm.yml](https://gitlab.com/khassel/magicmirror/-/blob/master/run/serveronly_mmpm.yml)

# Configuration of MMPM

After the first start you may have to update `MMPM_MAGICMIRROR_URI`, see [this wiki](https://github.com/Bee-Mar/mmpm/wiki/Status,-Hide,-Show-MagicMirror-Modules) for more information.

⚠️ The active modules page in the control center is not working until you restart both containers after a change of `MMPM_MAGICMIRROR_URI`. You can do this by running `docker-compose up -d --force-recreate` from inside the `~/magicmirror/run` folder.

⚠️ You may have to load MagicMirror in the browser before loading MMPM-GUI (or refresh MMPM-GUI) to see active modules.
