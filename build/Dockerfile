ARG PYTH_VERSION
ARG DEBIAN_VERSION
FROM python:${PYTH_VERSION}-slim-${DEBIAN_VERSION}

ARG PYTH_VERSION
ENV PYTH_VERSION=${PYTH_VERSION}

ARG NODE_VERSION
ARG MMPM_VERSION
ARG TARGETPLATFORM
RUN <<EOF
set -ex
groupadd --gid 1000 node
useradd --uid 1000 --gid node --shell /bin/bash --create-home node
_pck="curl ca-certificates xz-utils git sudo nano"
if [ "${TARGETPLATFORM}" = "linux/arm/v7" ]; then
  _pck="${_pck} libatomic1 build-essential"
  pip config set global.extra-index-url https://www.piwheels.org/simple
fi
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -qy --no-install-recommends install ${_pck}
dpkgArch="$(dpkg --print-architecture)"
case "${dpkgArch##*-}" in
  amd64) ARCH='x64';;
  ppc64el) ARCH='ppc64le';;
  s390x) ARCH='s390x';;
  arm64) ARCH='arm64';;
  armhf) ARCH='armv7l';;
  i386) ARCH='x86';;
  *) echo "unsupported architecture"; exit 1 ;;
esac
curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz"
tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner
rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz"
ln -s /usr/local/bin/node /usr/local/bin/nodejs
node -v
npm -v
python3 -m pip install --no-cache-dir --target /home/node/.local/lib/python${PYTH_VERSION}/site-packages mmpm==${MMPM_VERSION}
if [ "${TARGETPLATFORM}" = "linux/arm/v7" ]; then
  apt-get remove -y build-essential
  apt-get autoremove -y
fi
apt-get clean
rm -rf /var/lib/apt/lists/*
echo "node ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
chown -R node:node /home/node
EOF

USER node

WORKDIR /home/node

COPY --chown=node:node mmpm-env.json entrypoint.sh /home/node/
ENV PATH=$PATH:/home/node/.local/lib/python${PYTH_VERSION}/site-packages/bin

ARG ELECTRON_VERSION
ARG TARGETPLATFORM
RUN <<EOF
set -ex
chmod +x /home/node/entrypoint.sh
mkdir MagicMirror
if [ "${TARGETPLATFORM}" != "linux/amd64" ]; then
  cd MagicMirror
  npm install electron@${ELECTRON_VERSION}
fi
EOF

EXPOSE 7890

ENTRYPOINT ["/home/node/entrypoint.sh"]
