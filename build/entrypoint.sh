#!/bin/bash

echo "correct permissions to mounted dir ..."
sudo chown -R node:node /home/node/.config/
mkdir -p /home/node/.config/mmpm

if [[ -f /home/node/.config/mmpm/mmpm-env.json ]]; then
  # set MMPM_IS_DOCKER_IMAGE to true:
  sed -i -r 's|"MMPM_IS_DOCKER_IMAGE": .*|"MMPM_IS_DOCKER_IMAGE": true,|g' /home/node/.config/mmpm/mmpm-env.json
else
  cp /home/node/mmpm-env.json /home/node/.config/mmpm/mmpm-env.json
fi

echo "check if config.js contains mmpm module section ..."
if [[ -f /home/node/MagicMirror/config/config.js ]]; then
  [[ "$(cat /home/node/MagicMirror/config/config.js | grep 'module: "MMM-mmpm"')" ]] || sed -i 's|modules: \[|modules: \[{ module: "MMM-mmpm" },|g' /home/node/MagicMirror/config/config.js
fi

cp /home/node/.config/mmpm/mmpm-env.json /tmp/mmpm-env.json

echo "check if MMM-mmpm is installed in modules folder ..."
if [[ ! -d /home/node/MagicMirror/modules/MMM-mmpm ]]; then
  mmpm install MMM-mmpm -y || true
fi

[ -z "$TZ" ] && export TZ="$(curl -s http://geoip.ubuntu.com/lookup | sed -n -e 's/.*<TimeZone>\(.*\)<\/TimeZone>.*/\1/p')"

if [ -z "$TZ" ]; then
  echo "***WARNING*** could not set timezone, please set TZ variable in docker-compose.yml, see https://gitlab.com/khassel/magicmirror#timezone"
else
  echo "timezone is $TZ"
  sudo ln -fs /usr/share/zoneinfo/$TZ /etc/localtime
  sudo sh -c "echo $TZ > /etc/timezone"
fi

echo "starting gunicorn processes ..."

python3 -m gunicorn -k gevent -b 0.0.0.0:7891 mmpm.wsgi:app --daemon
python3 -m gunicorn -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker -w 1 'mmpm.log.server:create()' -b 0.0.0.0:6789 --daemon
python3 -m gunicorn -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker -w 1 'mmpm.api.repeater:create()' -b 0.0.0.0:8907 --daemon

cat /home/node/.config/mmpm/mmpm-env.json
echo ""

echo "starting webserver ..."
python3 -m http.server -d /home/node/.local/lib/python${PYTH_VERSION}/site-packages/mmpm/ui -b 0.0.0.0 7890
